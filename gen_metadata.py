from gensim.models import Word2Vec

if __name__ == '__main__':
    model = Word2Vec.load('./w2v-jawiki/jawiki.model')

    fin = open('./mecab-ipadic-neologd/seed/'
               'neologd-common-noun-ortho-variant-dict-seed.20150323.csv',
               'r', encoding='utf-8')
    fout = open('./metadata.txt', 'w', encoding='utf-8')

    count = 0
    while count < 50000:
        # 辞書のエントリは，次のようなフォーマットで表現される
        # 表層形,左文脈ID,右文脈ID,コスト,品詞,品詞細分類1,品詞細分類2,品詞細分類3,活用形,活用型,原形,読み,発音
        # 詳細は，https://taku910.github.io/mecab/dic.html
        line = fin.readline()
        entry = line.split(',')

        # エントリの原形がWord2Vecモデルのボキャブラリに存在する場合
        # Word2Vecのベクトル空間における最近傍の単語を類義語として取り出す
        if entry[10] in model:
            synonym = model.most_similar(entry[10], topn=1)
            fout.writelines('%s\t%s\n' % (entry[10], synonym[0][0]))
            count += 1

    fin.close()
    fout.close()
