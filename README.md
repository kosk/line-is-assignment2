# LINE社 IS選考課題

## 制作物一覧
1. NEologdに含まれる単語に関するメタ情報の収集プログラム
  - Word2Vecを用いたメタ情報（類義語）の収集
  - Github: https://github.com/k0sk/line-is-assignment2

2. NEologdを用いたWebサービス
  - NEologdとWord2Vecを用いたゲームボットの実装
  - Github: https://github.com/k0sk/magical-banana
  - URL: http://www.kosk.me/magical-banana-front

## メタ情報の収集
[NEologd](https://github.com/neologd/mecab-ipadic-neologd)に含まれる辞書ファイル ```neologd-common-noun-ortho-variant-dict-seed.20150323.csv``` の先頭5万件のエントリについて，Word2Vecを用いてメタ情報（類義語）の収集を行いました．

### ダウンロード
収集結果のデータは，下記のリンクからダウンロードできます．

- [metadata.txt](https://github.com/k0sk/line-is-assignment2/releases/download/v1.0/metadata.txt)
- [metadata_uniq.txt](https://github.com/k0sk/line-is-assignment2/releases/download/v1.0/metadata_uniq.txt)

*※ ```metadata_uniq.txt``` は， ```metadata.txt``` から重複を排除したデータです．*
